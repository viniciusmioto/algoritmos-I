program series003;
var
    s: real;
    num, den, cont: integer;
begin
    s := 0;
    den := 1;
    num := 1;
    cont := 1;
    s := num/den;
    while cont < 5 do
    begin
        num := den + num;
        den := num + den;
        writeln(num, ' ', den);
        s := s + num / den;
        cont := cont + 1;
    end;
    writeln(s:0:2);
end.
