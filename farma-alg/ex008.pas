program ex008;
var
    n, ant, soma, quant: longint;
begin
    read(ant, n);
    soma := n + ant;
    quant := 2;
    while (n * 2 <> ant) and (n / 2 <> ant) do
    begin
        ant := n;
        read(n);
        soma := soma + n;
        quant := quant + 1;
    end;
    writeln(quant, ' ', soma, ' ', ant, ' ', n);
end.
