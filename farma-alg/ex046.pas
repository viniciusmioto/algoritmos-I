program ex46;
var
    n, numerador, media_ponderada: real;
    p, denominador: integer;
begin
    read(n, p);
    numerador := 0;
    denominador := 0;
    while (n <> 0) or (p <> 0) do
    begin
        numerador := numerador + n * p;
        denominador := denominador + p;
        read(n, p);
    end;
    media_ponderada := numerador / denominador;
    writeln(media_ponderada:0:2);
end.         
