program ex103;
var
    cosseno, seno, tangente, distancia, altura: real;
begin
    readln(cosseno, distancia);
    seno := sqrt(1 - (cosseno * cosseno));
    tangente := seno / cosseno;
    altura := distancia * tangente;
    writeln(altura:0:3);
end.
