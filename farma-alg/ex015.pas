program ex015;
var
    n, n_mult, digito, soma: integer;
begin
    read(n);
    n_mult := n * 37;
    while n_mult > 0 do
    begin
        digito := n_mult mod 10;
        n_mult := n_mult div 10;
        soma := soma + digito;
    end;
    if soma = n then
        writeln('SIM')
    else
        writeln('NAO');
end.
