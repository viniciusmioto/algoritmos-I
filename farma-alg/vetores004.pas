program vetores004;
const
    MAX = 200;
    
type
    tpVetor = array[0..MAX] of integer;

var
    v, distintos, ocorrencias: tpVetor;
    n, tamDist: integer;
    
procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
    begin
        read(v[i]);
        ocorrencias[v[i]] := ocorrencias[v[i]] + 1;
    end;
end;

function busca(var v:tpVetor; valor, tam: integer): integer;
var     
    i: integer;
begin
    v[0] := valor;
    i:= tam;
    while v[i] <> valor do
    begin
        i := i - 1;
    end;
    busca := i;
end;

procedure preencheDistintos(var distintos, v: tpVetor; tam: integer; var tamDist: integer);
var i, j, zeros: integer;
begin
    j := 0;
    zeros := 0;
    for i := 1 to tam do
    begin
        if (busca(distintos, v[i], tam) = 0) xor ((v[i] = 0) and (zeros = 0)) then
        begin
            j := j + 1;
            distintos[j] := v[i];
            if v[i] = 0 then
                zeros := 1;
        end;
    tamDist := j;   
    end;
end;

procedure escreveVetor(var distintos: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        write(distintos[i], ' ');
    writeln();
end;    

procedure escreveOcorrencias(var distintos, ocorrencias: tpVetor; tamDist: integer);
var i: integer;
begin
    for i := 1 to tamDist do
        if  ocorrencias[distintos[i]] = 1 then
            writeln(distintos[i], ' ocorre ', 1, ' vez')
        else
            writeln(distintos[i], ' ocorre ', ocorrencias[distintos[i]], ' vezes');
end;

begin
    read(n);
    leVetor(v, n);
    preencheDistintos(distintos, v, n, tamDist);
    if n = 0 then
        writeln('vetor vazio')
    else if tamDist > 1 then
        write('a sequencia tem ', tamDist, ' numeros distintos: ')
    else
          write('a sequencia tem 1 numero distinto: ');
    escreveVetor(distintos, tamDist);
    escreveOcorrencias(distintos, ocorrencias, tamDist);
end.
