program vetores006;
const
    MAX = 200;
    
type
    tpVetor = array[0..MAX] of real;
    
var
    tam, vez: integer;
    valor, operacao: real;
    v: tpVetor;
    
procedure escreveVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    if tam <> 0 then
    begin
        for i:= 1 to tam do
            write(v[i]:0:1, ' ');
        writeln();
    end
    else
        writeln('vazio');
end;      
    
function busca(var v:tpVetor; valor: real; tam: integer): integer;
var     
    i: integer;
begin
    v[0] := valor;
    i:= tam;
    while v[i] <> valor do
    begin
        i := i - 1;
    end;
    busca := i;
end;
    
procedure insereVetor(var v: tpVetor; var tam: integer);
var 
    i: integer;
    valor: real;
begin    
    read(valor);
    i := tam;
    if tam = 0 then
    begin
        v[1] := valor;
        tam := tam + 1;
        escreveVetor(v, tam);
    end
    else if tam < MAX then
    begin
        v[0] := valor;
        while v[i] > valor do
        begin
            v[i+1] := v[i];
            i := i - 1;
        end;
        v[i+1] := valor;
        tam := tam + 1;
        escreveVetor(v, tam);
    end
    else
        writeln('erro');
end;
    
procedure removeVetor(var v: tpVetor; var tam: integer);
var i, indice_valor: integer;
begin
    read(valor);
    indice_valor := busca(v, valor, tam);
    if indice_valor <> 0 then   
        begin   
            for i := indice_valor to tam do
                v[i] := v[i+1];
             tam := tam - 1;
             escreveVetor(v, tam);
        end
    else
        writeln('erro');
   
end;      
    
begin
    tam := 0;
    vez := 0;
    read(operacao);
    while operacao <> 0 do
    begin
        if operacao = 1 then
            insereVetor(v, tam)
        else if operacao = 2 then
        begin
            if tam <> 0 then
                removeVetor(v, tam)
            else if vez = 0 then
                writeln('erro')
            else
                writeln('vazio');
        end;
        read(operacao);
     end;
     escreveVetor(v, tam);
     vez := vez + 1;
end.
