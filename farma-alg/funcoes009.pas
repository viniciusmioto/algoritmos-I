program media_alunos;
var i, n, p1, p2, p3, media: longint;

function media_ponderada(p1, p2, p3: integer): integer;
begin
    if (p1 >= 0) and (p1 <= 100) and (p2 >= 0) and (p2 <= 100) and (p3 >= 0) and (p3 <= 100) then
        media_ponderada := (p1 + p2 * 2 + p3 * 3) div 6
    else
        media_ponderada := 0;
end;

function aprovado(media: integer):boolean;
begin
    if media >= 50 then
        aprovado := true
    else
        aprovado := false;
end;

begin
    read (n);
    for i:= 1 to n do
    begin
        read (p1, p2, p3);
        media:= media_ponderada (p1, p2, p3);
        if aprovado (media) then
            writeln ('aluno ',i,' aprovado com media: ', media)
        else
            writeln ('aluno ',i,' reprovado com media: ', media);
    end;
end.

