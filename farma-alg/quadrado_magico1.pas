program quadrado_magico1;

type
    matriz = array[1..100, 1..100] of longint;
    
var
    n: integer;
    m: matriz;    
    
procedure le_matriz(var m: matriz; n: integer);
var l, c: integer;
begin
    for l:= 1 to n do
        for c:= 1 to n do
            read(m[l, c]);
end;    
    
procedure escreve_matriz(var m: matriz; n:integer);
var l, c: integer;
begin
    for l:= 1 to n + 1 do
    begin
        for c := 1 to n + 1 do
            write(m[l, c], ' ');
        writeln();
    end;    
end;
    
function verifica_quad(var m: matriz; n: integer): boolean;
var 
    l, c: integer;
    verifica: boolean;
begin
    verifica := true;
    for l:= 1 to n do
    begin
        for c:= 1 to n do
        begin
            m[l, n+1]  := m[l, n+1] + m[l, c]; 
            m[n+1, c] := m[n+1, c] + m[l, c];
            if c = l then
                m[n+1, n+1] := m[n+1, n+1] + m[l, c];
        end;
    end;
    
    l := n + 1;
    for c := 1 to n do
        if m[l, c] <> m[l, c + 1] then
            verifica := false; 
    
    c := n + 1;
    for l := 1 to n do
        if m[l, c] <> m[l+1, c] then
            verifica := false;
    
    verifica_quad := verifica;
            
end;    

begin
    read(n);
    le_matriz(m, n);
    if verifica_quad(m, n) then
        writeln('sim')
    else writeln('nao');
end.
