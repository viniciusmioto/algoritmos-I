program repet_mat;

type
    matriz = array[1..100, 1..100] of integer;

var
    n, m: integer;
    mat: matriz;
    
procedure le_matriz(var mat: matriz; lin, col: integer);
var l, c: integer;
begin
    for l := 1 to lin do
        for c := 1 to col do
            read(mat[l, c]);
end;    
    
procedure escreve_matriz(var mat: matriz; lin, col: integer);
var l, c: integer;
begin
    for l := 1 to lin do
    begin
        for c:= 1 to col do
            write(mat[l, c], ' ');
        writeln();
    end;
end;    

function procura_repetidos(var mat: matriz; lin , col: integer): boolean;
var 
    l, c, el, ec: integer;
    encontrou: boolean;
begin
    encontrou := false;
    el := 1;
    ec := 1;
    l := 1;
    while not(encontrou) and (el <= lin) do
    begin 
        ec := 1;
        while not(encontrou) and (ec <= col) do
        begin
            for l := 1 to lin do
                for c := 1 to col do
                    if (mat[el, ec] = mat [l, c]) and ((l <> el) or (c <> ec)) then
                        encontrou := true; 
            ec := ec + 1;
        end;
        el := el + 1;
    end;
    procura_repetidos := encontrou;
end;
    
begin
    read(n, m);
    if (n >= 1) and (n <= 100) and (m >= 1) and (m <= 100) then
    begin    
        le_matriz(mat, m, n);
        if procura_repetidos(mat, m, n) then
            writeln('sim')
        else
            writeln('nao');
    end;
end.
