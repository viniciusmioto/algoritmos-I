program ex022;
var
    x, y: integer;
    quadrante: char;
begin
    readln(x, y);
    if (x > 0) and (y > 0) then
        quadrante := '1'
    else if (x > 0) and (y < 0) then
        quadrante := '4'
    else if (x < 0) and (y > 0) then
        quadrante := '2'
    else if (x < 0) and (y < 0) then
        quadrante := '3'
    else if (x = 0) and (y <> 0) then
        quadrante := 'Y'
    else if (x <> 0) and (y = 0) then
        quadrante := 'X'
    else
        quadrante := 'O';
    writeln(quadrante);
end.
            
