program elem_nulos;

const
    MAX = 100;
    
type
    matriz = array[1..MAX, 1..MAX] of integer;    

var
    n, m, qtdLin, qtdCol: integer;
    mat: matriz;
    
procedure le_matriz(var mat: matriz; lin, col: integer);
var l, c: integer;
begin
    for l:= 1 to lin do
        for c:= 1 to col do
            read(mat[l, c]);
end;

procedure conta_nulos(var mat: matriz; lin, col: integer; var qtdLin, qtdCol: integer);
var l, c, l_nulo, c_nulo: integer;
begin    
    qtdLin := 0;
    qtdCol := 0;
    for c:= 1 to col do
    begin
        c_nulo := 0;
        for l:= 1 to lin do
        begin
            if mat[l, c] = 0 then
                c_nulo := c_nulo + 1;   
        end;    
        if c_nulo = lin then
                qtdCol := qtdCol + 1;
    end;
    
    for l:= 1 to lin do
    begin
        l_nulo := 0;
        for c:= 1 to col do
        begin
            if mat[l, c] = 0 then
                l_nulo := l_nulo + 1;
        end;
        if l_nulo = col then
                qtdLin := qtdLin + 1;
    end;
end;

begin
    read(n, m);
    if (n >= 1) and (n <= 100) and (m >= 1) and (m <= 100) then
    begin
        le_matriz(mat, m ,n);
        conta_nulos(mat, m ,n, qtdLin, qtdCol);
        writeln('linhas: ', qtdLin);
        writeln('colunas: ', qtdCol);
    end;
end.
