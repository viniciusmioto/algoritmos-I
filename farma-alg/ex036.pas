program ex036;
const
    OPCAO1 = (50 * 40)/1000;
    OPCAO2 = (50 * 60)/1000;
    OPCAO3 = (50 * 80)/1000;
var
    opcao, area: integer;
    caixas: real;
begin
    readln(opcao, area);
    if opcao = 1 then
        caixas := area / OPCAO1
    else if opcao = 2 then
        caixas := area / OPCAO2
    else
        caixas := area / OPCAO3;
    writeln(round(caixas), ' caixas');
end.
