program ex089;
const
    perc_distribuidor = 28/100;
    impostos = 45/100;
var
    custo_fabrica, custo_consumidor: real;
begin
    readln(custo_fabrica);
    custo_consumidor := custo_fabrica + (custo_fabrica * perc_distribuidor) + (custo_fabrica * impostos);
    writeln(trunc(custo_consumidor));
end.
