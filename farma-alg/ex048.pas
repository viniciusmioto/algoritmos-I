program ex48;
var
    produto: longint;
    a, b: integer;
begin
    readln(a, b);
    if (a > 0) and (b mod 2 = 1) and (a mod 2 = 1) and (a <= b) then
    begin
        produto := 1;
        while a <= b do
        begin   
            produto := produto * a;
            a := a + 2;
        end;
        writeln(produto);
    end
    else
        writeln('erro');
end.
