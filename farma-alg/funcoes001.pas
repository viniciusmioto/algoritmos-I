program funcoes001;
var n, m: longint;

function contrario(n, m: longint): boolean;
var
    invertido: longint;
begin
    invertido := m mod 10;
    m := m div 10;
    while m <> 0 do
    begin
        invertido := invertido * 10 + (m mod 10);
        m := m div 10;
    end;
    if n = invertido then
        contrario := true
    else
        contrario := false;
end; 

begin
read (n,m);
if contrario (n,m) then
writeln (n,' eh o contrario de ',m)
else
writeln (n,' nao eh o contrario de ',m);
end.
