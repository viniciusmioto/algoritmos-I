program ex033;
var
    num_lados, medida: integer;
begin   
    readln(num_lados, medida);
    if num_lados = 3 then
    begin
        write('TRIANGULO ');
        writeln(medida * 3);
    end
    else if num_lados = 4 then
    begin
        write('QUADRADO ');
        writeln(medida * medida);
    end
    else if num_lados = 5 then
        writeln('PENTAGONO')
    else writeln('ERRO');
end.
