program permutacao_matriz;

type matriz = array[1..100, 1..100] of byte;

var
    n: integer;
    mat: matriz;
    
procedure le_matriz(var mat: matriz; n: integer);
var l, c: integer;
begin
    for l:= 1 to n do
        for c:= 1 to n do
            read(mat[l, c]);
end;    
    
function verifica_permuta(var mat: matriz; n: integer): boolean;
var 
    l, c, qtd_zero, qtd_um: integer;
    verifica_linha, verifica_coluna: boolean;
begin
    verifica_linha := true;
    verifica_coluna := true;
    for l:= 1 to n do
    begin
        qtd_zero := 0;
        qtd_um := 0; 
        for c:= 1 to n do
        begin
            if mat[l, c] = 0 then
                qtd_zero := qtd_zero + 1
            else if mat[l, c] = 1 then
                qtd_um := qtd_um + 1
            else
                verifica_linha := false;
        end;
        if (qtd_zero <> n - 1) or (qtd_um <> 1) then
            verifica_linha := false;
    end;
    
    for c := 1 to n do
    begin
        qtd_zero := 0;
        qtd_um := 0;
        for l := 1 to n do
        begin
            if mat[l, c] = 0 then
                qtd_zero := qtd_zero + 1
            else if mat[l, c] = 1 then
                qtd_um := qtd_um + 1
            else
                verifica_coluna := false;
        end;
        if (qtd_zero <> n - 1) or (qtd_um <> 1) then
            verifica_coluna := false;
    end;    
    verifica_permuta := verifica_linha and verifica_coluna; 
end;    
    
begin
    read(n);
    if (n >= 1) and (n <= 100) then
    begin
        le_matriz(mat, n);
        if verifica_permuta(mat, n) then
            writeln('sim')
        else writeln('nao');
    end;
end.
    
