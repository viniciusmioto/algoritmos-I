program vetores.pas;
type
    tpVetor = array[1..500] of integer;
    
var
    tam, valor: integer;
    v: tpVetor;
    
procedure leVetor(var v: tpVetor; var tam: integer);
var i, n: integer;
begin
    read(n);
        i := 1;
        while n <> 0 do
        begin
            if n <> 0 then
            begin    
                v[i] := n;
                i := i + 1;
            end;
            read(n);
             tam := i;
        end;
end;    
    
function busca(n: integer; var v: tpVetor; tam: integer): integer;
var i: integer;
begin
    v[tam+1] := n;
    i := 1;
    while v[i] <> n do
        i := i + 1;
    if i <= tam then
        busca := i
    else
        busca := 0;
end;

begin
    tam := 0;
    leVetor(v, tam);
    if tam <> 0 then
    begin
        read(valor);
        while valor <> 0 do
        begin
            writeln(busca(valor, v, tam));
            read(valor);
        end;
    end
    else
        writeln('vetor vazio');
end.
