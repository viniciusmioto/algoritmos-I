program vetores001;
const 
    MIN = 0;
    MAX = 200;
type
    tpVetor = array[MIN..MAX] of real;
var
    v: tpVetor;
    n: integer;

procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;
    
procedure resultadoDivisao(var v: tpVetor; tam: integer);
var i: integer;
somaPositivosPar, somaNegativosImpar: real;
begin
    somaNegativosImpar := 0;
    somaPositivosPar := 0;
    for i:= 1 to tam do
    begin
        if (i mod 2 = 0) and (v[i] > 0) then
            somaPositivosPar := somaPositivosPar + v[i]
        else
            if (i mod 2 <> 0) and (v[i] < 0) then
                somaNegativosImpar := somaNegativosImpar + v[i];   
    end;
    if tam = 0 then
        writeln('vetor vazio')
    else if somaNegativosImpar <> 0 then
        writeln((somaPositivosPar / somaNegativosImpar):0:2)
    else
        writeln('divisao por zero');
end;

begin
    read(n);
    leVetor(v, n);
    resultadoDivisao(v, n);
end.
