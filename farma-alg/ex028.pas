program ex028;
var
    fahrenheit: integer;
    celsius: real;
begin
    readln(fahrenheit);
    celsius:= (fahrenheit - 32) * 5/9;
    writeln(celsius:0:2);
    if celsius >= 100 then
        writeln('gasoso')
    else if (celsius <= 0) then
        writeln('solido')
    else
        writeln('liquido');
end.
        
