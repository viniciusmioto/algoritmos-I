program series001;
var
    s: real;
    num, den, cont, n, sinal: integer;
begin
    read(n);
    cont := 1;
    num := 1000;
    den := 1;
    sinal := 1;
    s := num / den;
    while cont < n do
    begin
        num := num - 3;
        den := den + 1;
        sinal := -sinal;
        s := s + sinal * (num/den);
        cont := cont + 1;
    end;
    if n = 0 then
        writeln(0)
    else
        writeln(s:0:2);
end.
