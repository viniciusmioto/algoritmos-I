program ex035;
var
    salario: real;
begin
    readln(salario);
    if salario < 540 then
        writeln('NAO')
    else if salario <= 1424 then
    begin
        write(1, ' ');
        writeln('0.00');
    end
    else if salario <= 2150 then
    begin
        write(2, ' ');
        writeln((salario * 0.075):0:2);
    end
    else if salario <= 2866 then
    begin
        write(3, ' ');
        writeln((salario * 0.15):0:2);
    end
    else if salario <= 3582 then
    begin
        write(4, ' ');
        writeln((salario * 0.225):0:2);
    end
    else 
    begin
        write(5, ' ');
        writeln((salario * 0.275):0:2);
    end;
end.
