program ex032;
var
    salario_atual, novo_salario, diferenca, percentual: real;
    codigo: integer;
begin
    readln(salario_atual, codigo);
    if codigo = 101 then
        percentual := 1.1
    else if codigo = 102 then
        percentual := 1.2
    else if codigo = 103 then
        percentual := 1.3
    else
        percentual := 1.4;
    novo_salario := salario_atual * percentual;
    diferenca := novo_salario - salario_atual;
    writeln(salario_atual:0:2);
    writeln(novo_salario:0:2);
    writeln(diferenca:0:2);
end.
