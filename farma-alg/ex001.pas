program ex1;
var
    n, dezena, unidade, cont: integer;
begin
    cont := 1;
    while cont <= 10 do
    begin
        readln(n);
        if (n <= 99) and (n >= 10) then
        begin
            dezena := n div 10;
            unidade := n mod 10;
            writeln(dezena * unidade);
            cont := cont + 1;
        end;
    end;
end.
