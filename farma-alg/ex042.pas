program ex042;
var 
    n, soma, quantidade: longint;
    media: real;
begin
    read(n);
    soma := 0;
    quantidade := 0;
    media := 0;
    while n <> 0 do
    begin
        quantidade := quantidade + 1;
        soma := soma + n;
        read(n);
        media := soma / quantidade;
    end;
    writeln(media:0:2);
end.
