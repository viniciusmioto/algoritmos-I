program ex54;
var
    lado, verificador, qtd_lados: integer;
    lados_iguais: boolean;
begin
    read(lado);
    verificador := lado;
    qtd_lados := 0;
    lados_iguais := true;
    while lado > 0 do
    begin
        qtd_lados := qtd_lados + 1; 
        read(lado);
        if (lado <> verificador) and (lado > 0) then
            lados_iguais := false;
    end;
    if (qtd_lados >= 3) and lados_iguais then
        writeln('SIM')
    else
        writeln('NAO');
end.
