program ex006;
var
    n, soma: longint;
    k, divisor, cont: integer;
begin
    read(k);
    cont := 1;
    n := 6;
    while cont <= k do
    begin
        soma := 0;
        divisor := 1;
        while (divisor < n)  do
        begin
            if (n mod divisor) = 0 then
                soma := soma + divisor;
            divisor := divisor + 1;
        end;
        if soma = n then
        begin
            write(n,' ');
            cont := cont + 1;  
        end; 
        n := n + 1;
    end;
end.
