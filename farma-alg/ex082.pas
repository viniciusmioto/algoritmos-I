program ex082;
var
    total_segundos, horas, minutos, segundos: longint;
begin
    readln(total_segundos);
    horas := total_segundos div 3600;
    minutos := (total_segundos div 60) mod 60;
    segundos := total_segundos mod 60;
    writeln(horas, ':', minutos, ':', segundos);
end.
