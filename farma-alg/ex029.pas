program ex029;
var
    i: integer;
    a, b, c, n1, n2, n3: real;
begin
    readln(i, a, b, c);
    if (a >= b) and (a >= c) then
        if b >= c then 
        begin
            n3 := a;
            n2 := b;
            n1 := c;
        end
        else
        begin
            n3 := a;
            n2 := c;
            n1 := b;
        end
    else if (b >= a) and (b >= c) then
        if a >= c then
        begin
            n3 := b;
            n2 := a;
            n1 := c;
        end
        else
        begin
            n3 := b;
            n2 := c;
            n1 := a;
        end
    else if (c >= a) and (c >= b) then
        if a >= b then
        begin
            n3 := c;
            n2 := a;
            n1 := b;
        end;
    if i = 1 then
        writeln(n1:0:0, ' ', n2:0:0, ' ', n3:0:0)
    else if i = 2 then
        writeln(n3:0:0, ' ', n2:0:0, ' ', n1:0:0)
    else
    begin
        if a = n3 then
            writeln(b:0:0, ' ', n3:0:0, ' ', c:0:0)
        else if b = n3 then
            writeln(a:0:0, ' ', n3:0:0, ' ', c:0:0)
        else
            writeln(a:0:0, ' ', n3:0:0, ' ', b:0:0);
    end;
end.
