program equacao_matriz;

type
    matriz = array[1..100, 1..100] of longint;
    vetor = array[1..100] of longint;

var
    lin, col: integer;
    m: matriz;
    resultado, lado: vetor;
    
procedure le_matriz(var m: matriz; lin, col: integer);
var l, c: integer;
begin
    for l:= 1 to lin do
        for c:= 1 to col do
            read(m[l, c]);
end;

procedure le_vetor(var v: vetor; n: integer);
var i: integer;
begin
    for i := 1 to n do
        read(v[i]);
end; 

function verifica_matriz(var m: matriz; lin, col: integer; var resultado, lado: vetor):boolean;
var 
    l, c, soma: integer;
    verifica: boolean;
begin
    verifica:= true;
    for l := 1 to lin do
    begin
        soma := 0;
        for c:= 1 to col do
            soma := soma + m[l, c] * resultado[c]; 
        if soma <>  lado[l] then
            verifica := false;
    end; 
end;
    
begin
    read(lin, col);
    le_vetor(resultado, col);
    le_matriz(m, lin, col);
    le_vetor(lado, lin);
    if verifica_matriz(m, lin, col, resultado, lado) then
        writeln('sim')
    else writeln('nao');
end.    
    
