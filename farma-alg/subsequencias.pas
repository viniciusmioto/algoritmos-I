program susequencias;

const MAX = 100;

type vetor = array [1..MAX] of longint;

var
    v : vetor;
    n, pos, tamanho_subsequencia: longint;

procedure ler_vetor (var v: vetor; n: longint);
var i: integer;
begin
    for i:= 1 to n do
        read(v[i]);
end;

function tem_subsequencia_iguais (var v: vetor; n, tam_seg: longint): longint;
(*
recebe uma subsequencia "v" que em tamanho "n" e procura por subsequencias
iguais de tamanho "tam_seg". A funcao devolve zero se nao encontrou
subsequencias iguais ou devolve a posicao do inicio da primeira subsequencia
que encontrou.
*)
var i, j, iguais, posicao, k: integer;
begin
    posicao := 0;
    for i := 1 to n - tam_seg + 1 do
    begin
        for j := tam_seg + 1 to n do
        begin
            iguais := 0;
            if (v[i] = v[j]) and (posicao = 0) then
            begin
                iguais := 1;
                for k := 1 to tam_seg do
                begin
                    if v[i+k] = v[j+k] then
                        iguais := iguais + 1
                    else if (iguais = tam_seg) and (posicao = 0) then
                    begin
                        posicao := i;
                        iguais := 0;
                    end;
                end;
            end;
        end;
    end;
    tem_subsequencia_iguais := posicao;
end;

(* programa principal *)
begin
    read (n);
    // tamanho da subsequencia a ser lido
    ler_vetor (v,n); 
    pos:= 0;
    tamanho_subsequencia:= n div 2; // inicia com maior valor possivel
    while (pos = 0) and (tamanho_subsequencia >= 2) do
    begin
         pos:= tem_subsequencia_iguais (v, n, tamanho_subsequencia);
         tamanho_subsequencia:= tamanho_subsequencia - 1;
    end;
    if pos > 0 then
        writeln (pos, ' ', tamanho_subsequencia+1)
    else
        writeln ('nenhuma');
end.

