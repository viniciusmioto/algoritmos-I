program matttriangular;

const MAX = 100;

type
    matriz = array[1..MAX, 1..MAX] of longint;

var
    n: integer;
    m: matriz;

procedure le_matriz(var m: matriz; n: integer);
var l, c: integer;
begin
    for l := 1 to n do
        for c := 1 to n do
            read(m[l, c]);
end;

function verifica_triangular(var m: matriz; n: integer): boolean;
var 
    l, c: integer;
    tri_superior, tri_inferior: boolean;
begin
    tri_superior := true;
    tri_inferior := true;
    l := 1;
    while (l < n) and (tri_inferior) do
    begin
        c := l+1;
        while (c <= n) and (tri_inferior) do
        begin
            if m[l, c] <> 0 then
                tri_inferior := false;
            c:= c + 1;
        end;
        l:= l + 1;
    end; 

    l := 2;
    while (l <= n) and (tri_superior) do
    begin
        c := 1;
        while (c < l) and (tri_superior) do
        begin
             if m[l, c] <> 0 then
                tri_superior := false;
             c := c + 1;
        end;
        l := l + 1;
    end;
    verifica_triangular := (tri_inferior or tri_superior); 
end;

begin
    read(n);
    if n < MAX then
    begin
        le_matriz(m, n);
        if verifica_triangular(m, n) then
            writeln('sim')
        else
            writeln('nao');
    end;
end.
