program ex003;
var
    val_quad: integer;
    n1, n2: longint;
begin
    val_quad := 1;
    read(n1, n2);
    while (n1 <> 0) and (n2 <> 0) do
    begin
        if n1 * n1 <> n2 then
            val_quad := 0;
        read(n1);
        if n1 = 0 then
            n2 := 0
        else
            read(n2);
    end;
    writeln(val_quad);
end.
