program ex023;
var
    lado1, lado2, lado3: integer;
begin
    readln(lado1, lado2, lado3);
    if (lado1 = lado2) and (lado2 = lado3) then
        writeln('Equilatero')
    else
        if (lado1 <> lado2) and (lado2 <> lado3) and (lado1 <> lado3) then
            writeln('Escaleno')
        else
            writeln('Isosceles');
end.
