program series002;
var
    s: real;
    num, den, n: integer;
begin
    read(n);
    num:= 0;
    den := n;
    s := 0;
    while den > 1 do
    begin
        num:= num + 1;
        den := n - num;
        s:= s + num/den;
    end;
    writeln(s:0:2);
end.
