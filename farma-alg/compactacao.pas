program compactacao;

type
    tpVetor = array[1..100] of integer;

var
    n, tamCompacto: integer;
    v, compacto: tpVetor;

procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;

function busca(n: integer; var v: tpVetor; tam: integer): integer;
var i: integer;
begin
    v[tam+1] := n;
    i := 1;
    while v[i] <> n do
        i := i + 1;
    if i <= tam then
        busca := i
    else
        busca := 0;
end;

procedure preencheCompacto(var v, compacto: tpVetor; tam: integer; var tamCompacto: integer);
var
    i, j: integer;
begin
    j := 0;
    for i := 1 to tam do
    begin
        if busca(v[i], compacto, tamCompacto) = 0 then
        begin
            j := j + 1; 
            compacto[j] := v[i];  
        end;
        tamCompacto := j;
    end;
end;

procedure escreveVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
        write(v[i], ' ');
end;    

begin
    read(n);
    while n <> 0 do
    begin
        leVetor(v, n);
        tamCompacto := 0;
        preencheCompacto(v, compacto, n, tamCompacto);
        write('O: ');
        escreveVetor(v, n);
        writeln();
        write('C: ');
        escreveVetor(compacto, tamCompacto);
        writeln();
        read(n);
    end;
end.
