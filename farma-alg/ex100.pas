program ex100;
var
    n, a, b: integer;
begin
    readln(n);
    a := n div 100;
    b := n mod 100;
    if (a + b) * (a + b) = n then
        writeln('SIM')
    else
        writeln('NAO');
end.
