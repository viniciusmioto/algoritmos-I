program testa_se_primo;
var i: longint;

function eh_primo(n: longint): boolean;
var
    divisores, cont: integer;
begin
    eh_primo := true;
    if (n mod 2 = 0) and (n <> 2) then
        eh_primo := false
    else
    begin
        cont := 2;
        while cont <= sqrt(n) do
        begin
            if n mod cont = 0 then
                eh_primo := false;
            cont := cont + 1;
        end;
    end;
end;    
    
begin
    for i:= 1 to 10000 do
        if eh_primo (i) then
            writeln (i);
end.
