program ex011;
var
    n, divisores, i: integer;
begin
    read(n);
    i := 2;
    divisores := 0;
    if ((n mod 2) = 0) and (n > 2) then
        divisores := 1
    else
    begin
        while i <= sqrt(n) do
        begin
            if n mod i = 0 then
                divisores := divisores + 1;
            i := i + 1;
        end;
    end;
    if n = 0 then
        divisores := 1;
    if divisores > 0 then
        writeln('NAO')
    else
        writeln('SIM');
end.
