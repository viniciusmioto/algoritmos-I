program ex039;
var
    n, cont, produto: longint;
begin
    readln(n);
    cont:= 1;
    produto:= 1;
    while cont <= n do
    begin
        produto := produto * cont * 2;
        cont := cont + 1;
    end;
    writeln(produto);
end.
