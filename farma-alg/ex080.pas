program ex080;
var
    x, y : integer;
    divisao : real;
begin
    readln(x, y);
    
    if (x <> 0) and (y <> 0) then
        begin
            divisao := x/y + y/x;
            writeln(divisao:0:3);
        end
    else
        begin
            divisao:= 0;
            writeln(divisao:0:3);
        end;           
end.
