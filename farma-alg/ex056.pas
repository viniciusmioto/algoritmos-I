program ex056;
var
    n, m, soma: integer;
begin
    read(n, m);
    n := n + 1;
    while n < m do
    begin
        if n mod 2 = 0 then
            soma := soma + n;
        n := n + 1;
    end;
    writeln(soma);
end.
