program maximizar_soma;

type
    tpVetor = array[1..100] of integer;

var
    tam, i: integer;
    result_soma, somaMaxima: longint;
    v: tpVetor;

procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;    
    
function maiorSoma(var v: tpVetor; tam, inicio: integer): longint;
var i, soma, max: integer;
begin
    max := v[inicio];
    soma := 0;
    for i := inicio to tam do
    begin
        soma := soma + v[i];
        if soma > max then
            max := soma;
    end;
    maiorSoma := max;
end;

begin
    read(tam);
    leVetor(v, tam);
    somaMaxima := v[1];
    for i := 1 to tam do
    begin
        result_soma := maiorSoma(v, tam, i);
        if somaMaxima < result_soma  then
            somaMaxima := result_soma;
    end;
    writeln(somaMaxima);
end.
