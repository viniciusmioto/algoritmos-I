program ex061;
var
    n, a, b: integer;
begin
    read(n);
    a := 1;
    while a < n do
    begin
        writeln(a, ' ', n - a);
        a := a + 1;
    end;
end.
