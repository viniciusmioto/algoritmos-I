program ex101;
var n, a, b, c: integer;
begin
    readln(n);
    a := n mod 10;
    b := (n div 10) mod 10;
    c := n div 100;
    writeln(a, b, c);
end.
