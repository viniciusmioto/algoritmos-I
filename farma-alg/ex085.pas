program ex085;
var
    volume, diametro, raio: real;
begin
    readln(diametro);
    raio := diametro / 2;
    volume := 4 * 3.14 / 3 * raio * raio * raio;
    writeln(volume:1:2);
end.
    
    
