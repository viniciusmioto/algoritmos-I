program ex092;
var
    p1, p2, p3: integer;
    resultado: real;
begin
    readln(p1, p2, p3);
    resultado := (p1 + 2 * p2 + 3 * p3)/(1+2+3);
    writeln(resultado:0:0);
end.
