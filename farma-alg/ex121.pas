program ex121;
var
    n, a1, an: longint;
    sn: real;
begin
    readln(n, a1, an);
    sn := ((a1 + an) * n) / 2;
    writeln(sn:0:0);
end.
