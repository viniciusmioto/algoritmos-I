program ex49;
const
    SALARIO_MIN = 450;
var
    salario, percent1, percent2, percent3, percent4: real;
    intervalo1, intervalo2, intervalo3, intervalo4, total: integer;
    quant_sal_min: real;
begin
    readln(salario);
    intervalo1 := 0;
    intervalo2 := 0;
    intervalo3 := 0;
    intervalo4 := 0;
    while salario > 0 do
    begin
        quant_sal_min := salario / SALARIO_MIN;
        if quant_sal_min < 4 then
            intervalo1 := intervalo1 + 1
        else if quant_sal_min < 10 then
            intervalo2 := intervalo2 + 1
        else if quant_sal_min <= 20 then
            intervalo3 := intervalo3 + 1
        else
            intervalo4 := intervalo4 + 1;
        readln(salario);
        if salario = 0 then
            break;
    end;
    total := intervalo1 + intervalo2 + intervalo3 + intervalo4;
    percent1 := (intervalo1/total) * 100;
    percent2 := (intervalo2/total) * 100;
    percent3 := (intervalo3/total) * 100;
    percent4 := (intervalo4/total) * 100;
    writeln(percent1:0:2);
    writeln(percent2:0:2);
    writeln(percent3:0:2);
    writeln(percent4:0:2);
end.
