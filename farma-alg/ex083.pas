program ex083;
var
    total_dias, anos, meses, dias: integer;
begin
    readln(total_dias);
    anos := total_dias div 365;
    meses := (total_dias div 30) mod 12;
    dias := total_dias mod 30;
    if total_dias = 365 then dias := 0;
    writeln(anos, ' ', meses, ' ', dias);
end.

