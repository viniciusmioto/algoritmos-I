program ex041;
var
    i, penult, n, ult, fib, soma: integer;
begin
    readln(n);
    i := 2;
    penult := 0;
    ult := 1;
    soma := 1;
    while i < n do
    begin
        fib := ult + penult;
        soma := soma + fib;
        penult := ult;
        ult := fib;
        i := i + 1;
    end;
    writeln(soma);
end.
