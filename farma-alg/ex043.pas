program ex043;
var
    n, i, soma: integer;
begin
    readln(n);
    i := 1;
    soma := 0;
    while i <= n*2 do
    begin
        soma := soma + i;
        i := i + 2;
    end;
    writeln(soma);
end.
        
