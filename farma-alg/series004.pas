program ex004;
var
    s: real;
    num, numAnt, den, cont: integer;
begin
    cont := 1;
    num := 1;
    den := 2;
    s := num / den;
    while cont < 10 do
    begin
        numAnt := num;
        num := den + 2;
        den := numAnt + 2;
        s := s + num / den;
        cont:= cont + 1;
    end;
    writeln(s:0:2);
end.
