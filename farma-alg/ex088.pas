program ex088;
var
    x, y, resultado : real;
begin
    readln(x, y);
    resultado := exp(3*ln(x)) + x * y;
    writeln(resultado:0:3);
end.
