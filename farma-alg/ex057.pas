program ex57;
var
    n, maior_mult: longint;
begin
    read(n);
    maior_mult := 0;
    while n <> 0 do
    begin
        if (n mod 7 = 0) and (n > maior_mult) then
            maior_mult := n;
        read(n);
    end;
    writeln(maior_mult);
end.
