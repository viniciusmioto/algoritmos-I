program ex050;
var
    n1, n2, qtd: integer;
begin
    read(n1, n2);
    qtd := 0;
    while n1 >= n2 do
    begin
        if n1 mod n2 = 0 then           
            qtd := qtd + 1
        else break;
        n1 := n1 div n2;
    end;
    writeln(qtd);
end.
