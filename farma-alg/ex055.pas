program ex055;
var
    n, count: integer;
    m: longint;
begin
    read(n, m);
    while m > 0 do
    begin
        if m mod 10 = n then
            count := count + 1;
        m := m div 10;
    end;
    if count > 0 then 
        writeln(count)
    else
        writeln('NAO');
end.
