program converte;
var n: longint;

function eh_binario(n: longint):boolean;
var 
    digito: integer;
    binario: boolean;
begin
    binario := true;
    while (n <> 0) and binario do
    begin
        digito := n mod 10;
        if (digito <> 0) and (digito <> 1) then
            binario := false;
        n := n div 10;
    end;
    eh_binario := binario;
end; 

function converte_em_decimal(n: longint):longint;
var
    decimal: longint; 
    potencia: integer;
begin
    potencia := 1;
    decimal := 0;
    if eh_binario(n) then
    begin
        while n <> 0 do
        begin
            decimal := decimal + (n mod 10) * potencia;
            n := n div 10;
            potencia := potencia * 2;
        end;
        converte_em_decimal := decimal;
    end
    else
        converte_em_decimal := n;
end; 

begin
    read (n);
    writeln (converte_em_decimal(n));
end.
