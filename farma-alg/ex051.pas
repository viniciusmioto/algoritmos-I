program ex051;
var
    soma: longint;
    n, i: integer;
begin
    read(n);
    i := 1;
    soma := 0;
    while i <= n do
    begin
        soma := soma + i*i*i;
        i := i + 1;
    end;
    writeln(soma);
end.
