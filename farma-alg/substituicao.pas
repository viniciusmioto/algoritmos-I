program substituicao;
type
    tpVetor = array[1..100] of integer;
    
var
    v1, v2, v3: tpVetor;
    tv1, tv2, tv3, pos_ini, pos_fim: integer;    

procedure ler_vetor(var v: tpVetor; var tam: integer);
var n, i: integer;
begin
    read(n);
    i := 0;
    while (n <> 0) and (i < 100) do
    begin
        i := i + 1;
        v[i] := n; 
        read(n);
    end;
    tam := i;
end;

function ocorre(var vetor1, vetor2: tpVetor; tam1, tam2: integer;
                    var pos_ini, pos_fim: integer): boolean;
var i, j, qtdRepet, cont: integer;
begin
    qtdRepet:= 0;
    cont := 0;
    i := 1;
    j := 1;
    pos_ini := -1;
    while j <= tam1 do
    begin
        if vetor1[j] = vetor2[i] then
        begin
            if pos_ini = -1 then
                pos_ini := j;
            cont := cont + 1;
            if i < tam2 then
                i := i + 1
            else
                i := 1;
            if cont = tam2 then
            begin
                qtdRepet := qtdRepet + 1;
                j := j - 1;
                cont := 0;
            end;
        end
        else 
        begin
            i := 1;
            cont := 0;
        end;
        j := j + 1;
    end;
    pos_fim := pos_ini + tam2 -1;
    ocorre := qtdRepet > 0;
end;

procedure substitui(var v3:tpVetor; tv3: integer; var v1: tpVetor; var tv1: integer; pos_ini, pos_fim: integer);
var i, j, acrescenta: integer;
begin
    if tv3 > (pos_fim - pos_ini) then
    begin
        acrescenta := tv3 - (pos_fim - pos_ini)-1;
        for i:= tv1 downto pos_ini do
            v1[i+acrescenta] := v1[i];
        tv1 := tv1 + acrescenta;     
        j:= 1;    
        for i:= pos_ini to pos_ini + tv3 -1 do
        begin    
            v1[i] := v3[j];
            j := j + 1;
        end;
    end
    else if tv3 < 1 then
    begin
         j := 1;
        for i := pos_fim + 1 to tv1 do
        begin
            v1[i - (pos_fim - pos_ini +1)] := v1[i];
            j := j + 1;
        end;
        tv1 := tv1 - (pos_fim - pos_ini) - 1; 
    end 
    else
    begin
        j := 1;  
        for i := pos_ini to pos_ini + tv3 - 1 do
        begin
            v1[i] := v3[j];
            j := j + 1;
        end;
        for i := pos_fim + 1 to tv1 do
            v1[i - (pos_fim - pos_ini)] := v1[i];
        tv1 := tv1 -  (pos_fim - pos_ini) - 1 + tv3;
    end;    
    
end;

procedure imprime_vetor(v: tpVetor; tam: integer);
var i: integer;
begin
    if tam > 0 then
        for i:= 1 to tam do
            write(v[i], ' ')
    else
        write('vazia');
    writeln();
end;

begin
    ler_vetor (v1,tv1);
    ler_vetor (v2,tv2);
    ler_vetor (v3,tv3);
    if ocorre (v1, v2, tv1, tv2, pos_ini, pos_fim) then
        substitui (v3, tv3, v1, tv1, pos_ini, pos_fim);
    imprime_vetor (v1, tv1);
end.
