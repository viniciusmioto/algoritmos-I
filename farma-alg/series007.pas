program series007;
var
    s: real;
    den, num, cont, sinal: integer;
begin
    num := 1;
    den := 20;
    sinal := -1;
    s := sinal * (num / den);
    cont := 1;
    while cont < 10 do
    begin
        cont := cont + 1;
        if cont mod 2 = 0 then
        begin
            num := (num + 2) * 10;
            den := (den div 10) + 2;
        end
        else
        begin
            num := (num div 10) + 2;
            den := (den + 2) * 10;
        end;
        sinal := -sinal;
        s := s + sinal * (num / den);
    end;
    writeln(s:0:2);
end.
