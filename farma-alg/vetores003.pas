program vetores003;
type
    tpVetor = array[1..200] of integer;

var
    n: integer;
    v: tpVetor;

procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
        read(v[i]);
end;

function estaOrdenado(var v: tpVetor; tam: integer): boolean;
var i: integer;
begin
    i:= 1;
    while (i < tam) and (v[i] <= v[i+1]) do
        i := i + 1;
    if i < tam then
        estaOrdenado := false
    else
        estaOrdenado := true;
end;

procedure escreveInverso(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := tam downto 1 do
        write(v[i], ' ');
end;

begin
    read(n);
    leVetor(v, n);
    if n = 0 then
        writeln('vetor vazio')
    else if estaOrdenado(v, n) then
        writeln('sim')
    else
        writeln('nao');
    escreveInverso(v, n);
end.
