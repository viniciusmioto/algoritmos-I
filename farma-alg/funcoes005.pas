program calcula_tangente;
var angulo, tg: double;

function fat(n: double): double;
begin
    if n = 1 then
        fat := 1
   else
        fat := n * fat(n-1);
end;

function pot(x, y: double):double;
var i, resultado: double;
begin
    resultado := 1;
    i := 1;
    while i <= y do
    begin
        resultado := resultado * x;
        i := i + 1;
    end;
    pot := resultado;
end;

function seno(x: double): double;
var
    numero, sinal, s: double;
begin
    s := x;
    numero := 3;
    sinal := -1;
    while numero <= 13 do
    begin        
        s := s + sinal * pot(x, numero)/fat(numero);
        numero := numero + 2;
        sinal := -sinal;
    end;    
    seno := s;
end;

function cosseno(x: double): double;
var
    numero, fatorial, sinal: double;
    cont: integer;
    s: double;
begin
    s := 1;
    numero := 2;
    sinal := -1;
    cont := 1;
    while numero <= 12 do
    begin        
        s := s + sinal * pot(x, numero)/fat(numero);
        numero := numero + 2;
        sinal := -sinal;
    end;    
    cosseno := s;
end;

function existe_tangente(x: double; var tg: double): boolean;
begin
    existe_tangente := true;
    tg := 0;
    if (seno(x)/cosseno(x)) <> 0 then
        tg := (seno(x)/cosseno(x))
    else
        existe_tangente := false;
end;

begin
    read (angulo);
    if existe_tangente(angulo, tg) then
        writeln (tg:0:5)
    else
        writeln ('nao existe tangente de ',angulo:0:5);
end.
