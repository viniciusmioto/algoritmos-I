program ocorrencias;

type
    tpVetor = array[1..200] of integer;

var
    n, m: integer;
    vetor1, vetor2: tpVetor;
    
procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;    
    
procedure escreveVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
        write(v[i], ' ');
    writeln();
end;    

function repeticoes(var vetor1, vetor2: tpVetor; tam1, tam2: integer): integer;
var i, j, qtdRepet, cont: integer;
begin
    qtdRepet:= 0;
    cont := 0;
    i := 1;
    j := 1;
    while j <= tam1 do
    begin
        if vetor1[j] = vetor2[i] then
        begin
            cont := cont + 1;
            if i < tam2 then
                i := i + 1
            else
                i := 1;
            if cont = tam2 then
            begin
                qtdRepet := qtdRepet + 1;
                j := j - 1;
                cont := 0;
            end;
        end
        else 
        begin
            i := 1;
            cont := 0;
        end;
        j := j + 1;
    end;
    repeticoes := qtdRepet;

end;
    
begin
    read(n, m);
    if m < n then
    begin
        leVetor(vetor1, n);
        leVetor(vetor2, m);
        writeln(repeticoes(vetor1, vetor2, n, m));
    end;
end.
