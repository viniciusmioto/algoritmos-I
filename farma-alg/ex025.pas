program ex025;
var
    idade, dia, mes, ano: integer;
begin
    readln(dia, mes, ano);
    idade := 2021 - ano;
    if ((dia > 29) and (mes = 04)) or (mes > 4) then
        idade := idade - 1;
    writeln(idade);
end.
