program ex010;
var
    n: longint;
    d1, d2, d3, cont, i: integer;
    triangular: boolean;
begin
    read(n);
    triangular := false;
    d1 := 1;
    d2 := 2;
    d3 := 3;
    while (d3 <= n) and not triangular do
    begin
        if (d1 * d2 * d3) = n then
            triangular := true;
        d1 := d2;
        d2 := d3;
        d3 := d3 + 1;
    end;
    if triangular or (n=0) then
        writeln(1)
    else 
        writeln(0);
end.
