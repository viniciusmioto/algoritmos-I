program ex099;
var
    anos, meses, dias, total_dias : longint;
begin
    readln(anos, meses, dias);
    total_dias := anos * 365 + meses * 30 + dias;
    writeln(total_dias);
end.
