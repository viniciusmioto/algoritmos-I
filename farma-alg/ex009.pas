program ex009;
var
    pA, pB: double;
    tA, tB: real;
    anos: integer;
begin
    read(pA, pB, tA, tB);
    if ((pA >= pB) and (tA >= tB)) or ((pB >= pA) and (tB >= tA)) then
       anos:= 0
    else 
    begin
        if pA < pB then
        begin
            while pA < pB do
            begin
                pA := pA * (1 + tA);
                pB := pB * (1 + tB);
                anos := anos + 1;
            end;
        end
        else
        begin
            while pB < pA do
            begin
                pA := pA * (1 + tA);
                pB := pB * (1 + tB);
                anos := anos + 1;
            end;
        end
   end;
   writeln(anos);
end.
