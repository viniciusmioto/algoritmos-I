program ex040;
var
    n, maior, menor: integer;
begin
    read(n);
    if n <> 0 then
    begin
        maior := n;
        menor := n;
        while n <> 0 do
        begin
            if maior < n then
                maior := n;
            if menor > n then
                menor := n;
            read(n);
        end;
    writeln(maior, ' ', menor);
    end;
end.
