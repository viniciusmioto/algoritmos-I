program ex030;
var
    codigo, quantidade: integer;
    preco: real;
begin
    readln(codigo, quantidade);
    if codigo = 1001 then
        preco := 5.32 * quantidade
    else if codigo = 1324 then
        preco := 6.45 * quantidade
    else if codigo = 6548 then
        preco := 2.37 * quantidade
    else if codigo = 987 then
        preco := 5.32 * quantidade
    else if codigo = 7623 then 
        preco := 6.45 * quantidade
    else 
    begin
        preco := 0;    
        writeln('ERRO');
    end;
    
    if preco <> 0 then
        writeln(preco:0:2);
end.

