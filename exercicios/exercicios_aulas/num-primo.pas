program aula8ex2;
var
    primo: boolean;
    n, i: integer;
begin
    readln(n);
    if n >= 1 then
    begin
        primo := true;
        i := 2;
        while i < n do
        begin
            if n mod i = 0 then
                primo := false;
            i := i + 1;
        end;
        if primo then
            writeln('primo')
        else
            writeln('nao primo');
    end;
end.
