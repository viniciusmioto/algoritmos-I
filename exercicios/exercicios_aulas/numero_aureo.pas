program numero_aureo;
const PRECISAO = 0.000000000000000001;
var
   aureo_ant, n_aureo: double;
   a, b: longint;
begin
    a := 1;
    b := 1;
    aureo_ant := -1;
    n_aureo := b/a;
    writeln(b, '/', a, ' = ', n_aureo);
    while abs(n_aureo - aureo_ant) >= PRECISAO do
    begin
        aureo_ant := n_aureo;
        b := a + b;
        a := b - a;
        n_aureo := b/a;
        writeln(b, '/', a, ' = ', n_aureo:0:15);
   end;
end.
