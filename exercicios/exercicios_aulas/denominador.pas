program denominador;
var
    n, i: integer;
begin
    readln(n);
    i := 1;
    while i <= n do
        begin
            writeln((1/i):0:2);
            i += 1;
        end;
end.
