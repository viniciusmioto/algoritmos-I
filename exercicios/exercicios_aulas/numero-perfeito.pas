program aula8ex4;
var
    n, i, soma: integer;
begin
    readln(n);
    i := 1;
    soma:= 0;
    while i < n do
    begin
        if n mod i = 0 then
            soma:= soma + i;
        i := i + 1;
     end;
     if soma = n then
        writeln('perfeito')
     else
        writeln('nao perfeito');
end.
