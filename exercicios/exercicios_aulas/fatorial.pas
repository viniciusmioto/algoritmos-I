program ex4;
var
    n, i, f: longint;
begin
    readln(n);
    i := 1;
    f := 1;
    while i <= n do
        begin
            writeln(i,'! = ', f);
            i := i + 1;
            f := f * i;
        end;
end.
