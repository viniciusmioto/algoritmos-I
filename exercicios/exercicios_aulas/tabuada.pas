program tabuada;
var
    n, i : integer;
begin
    readln(n);
    i := 1;
    while i <= 10 do
        begin
            writeln(n, ' * ', i, ' = ', n * i);
            i := i + 1;
        end;
end.
