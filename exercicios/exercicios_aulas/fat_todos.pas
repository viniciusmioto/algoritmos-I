program fat_todos;
//fatorial de todos até n
var
    i, n: integer;
    fat: longint;
begin
    read(n);
    i := 1;
    fat := 1;
    while i <= n do
    begin
        writeln('fat(', i, ') = ', fat);
        i := i + 1;
        fat := i * fat;
    end;
end.
