program aula19ex2;
const
    MIN = 1;
    MAX = 100;
type
    tpVetor = array[MIN..MAX] of integer;
var
    vN, vM: tpVetor;
    n, m: integer;
    
procedure lerVetor(var vetor: tpVetor; tamanho: integer);
var i, valor: integer;
begin
    for i:= 1 to tamanho do
    begin
        read(valor);
        vetor[i] := valor;  
    end;
end;
    
function repetiuSequencia(var v1: tpVetor; var v2: tpVetor; tam1, tam2: integer): integer;
var j, i, cont, qtdRepeticoes: integer;
begin
    i := 1;
    j := 1;
    cont := 0;
    qtdRepeticoes := 0;
    while j <= tam1 do
    begin
        if v1[j] = v2[i] then
        begin
            cont := cont + 1;
            if i < tam2 then
                i := i + 1
            else
                i := 1;
            if cont = tam2 then
            begin
                qtdRepeticoes := qtdRepeticoes + 1;
                cont := 0;
            end;
        end
        else 
        begin
            i := 1;
            cont := 0;
        end;
        j := j + 1;
    end;
    repetiuSequencia := qtdRepeticoes;
end;

begin
    read(n, m);
    if m < n then
    begin
        lerVetor(vN, n);
        lerVetor(vM, m);
        writeln(repetiuSequencia(vN, vM, n, m));
    end;
end.
