program cont_plato;
var
    n, n_ant, qtd_plato, cont: integer;
begin
    read(n_ant);
    read(n);
    if n = n_ant then
        cont := 1
    else
        cont := 0;
    qtd_plato := 0;
    while (n_ant <> 0) and (n <> 0) do
    begin
        read(n);
        while n = n_ant do
        begin
            cont := cont + 1;
            read(n);
        end;
        if cont >= 1 then
            qtd_plato := qtd_plato + 1;
        if n <> n_ant then
            cont:= 0;
        n_ant := n;
    end;
    writeln(qtd_plato);
end.
        
