program somaMax;
const
    MAXV = 200;

type
    tpVetor = array[1..MAXV] of integer;
    
var
    v: tpVetor;
    n, max, maxi, i: integer;
    
function maiorSoma(var v: tpVetor; tam, ini: integer): integer;
var
    i, max, soma: integer;
begin
    max := v[ini];
    soma := v[ini];
    for i:= (ini+1) to tam do
    begin
        soma := soma + v[i];
        if soma > max then
            max := soma;
    end;
    maiorSoma := max;
end;

procedure leVetor(var v: tpVetor; tam: integer);
var
    i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;    

begin
    read(n);
    leVetor(v, n);
    max := maiorSoma(v, n, 1);
    for i:= 2 to n do
    begin
        maxI := maiorSoma(v, n, i);
        if maxI > max then
            max := maxI;
    end;
    writeln(max);
end.
