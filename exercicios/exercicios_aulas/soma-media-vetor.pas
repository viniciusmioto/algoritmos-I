program aula18ex1;
const
    MIN = 1;
    MAX = 100;
    
type tpVetor = array[MIN..MAX] of longint;
var
    v: tpVetor;
    tam, i, mediaV, acimaMedia, igualMedia, abaixoMedia: integer;
    
procedure leVetor(var v: tpVetor; var tam: integer);
var i, n: integer;
begin
    read(n);
    i := 1;
    tam := 0;
    while (n <> 0) and (i < 100) do
    begin
        v[i] := n;
        i := i + 1;
        read(n);
    end;
    tam := i - 1;
end;

function somaVetor(var v: tpVetor; tam: integer): longint;
var
    soma: longint;
    i: integer;
begin
    soma := 0;
    for i := 1 to tam do
        soma := soma + v[i];  
    somaVetor := soma;
end;

function mediaVetor(var v: tpVetor; tam: integer): integer;
begin
    mediaVetor := somaVetor(v, tam) div tam;
end;

begin
    leVetor(v, tam);
    writeln(somaVetor(v, tam));
    mediaV := mediaVetor(v, tam);
    writeln(mediaV);
    acimaMedia := 0; 
    igualMedia := 0; 
    abaixoMedia := 0;
    for i:= 1 to tam do
    begin
        if v[i] > mediaV then
            acimaMedia := acimaMedia + 1
        else if v[i] = mediaV then
            igualMedia := igualMedia + 1
        else
            abaixoMedia := abaixoMedia + 1;
    end;
    writeln(acimaMedia, ' ', igualMedia, ' ', abaixoMedia);
end.















