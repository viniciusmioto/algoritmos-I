program aula19ex1;
var
    vetor: array['a'..'z'] of integer;
    letra, pos, i: char;
begin
    read(letra);
    pos := 'a';
    for i:= 'a' to 'z' do
        vetor[i] := 0;
    while letra <> '.' do
    begin
        vetor[letra] := vetor[letra] + 1;
        read(letra);
    end;
    for i:= 'a' to 'z' do
        if vetor[i] <> 0 then
            writeln(i, ' = ', vetor[i]); 
end.
