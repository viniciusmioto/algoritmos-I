program aula20ex1;
const
    MIN = 1;
    MAX = 1000;
    
type
    tpVetor = array[MIN..MAX] of integer;
        
var
    n, pos, i, j, k, l, w, seq: integer;
    sequencias, seqCompactas, tamanhos, tamCompacta: tpVetor;
        
procedure escreveSeq(var vetor, tamanhos: tpVetor; seq: integer);
var i: integer;
begin
    for i:= 1 to tamanhos[seq] do
        write(vetor[i], ' ');
end;
    
procedure preencheCompacta(valor, i: integer; var seqCompactas:tpVetor; var w: integer);  
var 
    j: integer;
    repetiu: boolean;
begin
    repetiu := false;
    for j:= i downto 1 do
        if valor = seqCompactas[j] then
            repetiu := true;
            
    if not(repetiu) then
    begin
        w := w + 1;
        seqCompactas[w] := valor;
    end;
end;
    
procedure limpaCompacta(var vetor: tpVetor);
var i: integer;
begin
    for i:= 1 to 100 do
        seqCompactas[i] := 0;
end;    
    
begin
    read(n);
    pos := 1;
    seq:= 0;
    while n <> 0 do
    begin
        i := 1;
        while i <= n do
        begin
            read(sequencias[pos]);
            pos := pos + 1;
            i := i + 1;
        end;
        seq := seq + 1;
        tamanhos[seq] := n;   
        read(n);
    end;
    pos := 1;
    k := 0;
    for j := 1 to seq do
    begin
        w := 0;
        write('O: ');
        for i:= 1 to tamanhos[j] do
        begin
            write(sequencias[pos], ' ');   
            k := k + 1;
            preencheCompacta(sequencias[pos], i, seqCompactas, w);
            pos := pos + 1;
        end;
        writeln();
        write('C: ');
        for l := 1 to w do
            write(seqCompactas[l], ' ');
        writeln();
        limpaCompacta(seqCompactas);
    end;
    
end.







