program aula18ex2;
const
    MIN = 1;
    MAX = 9;

type tpVetor = array [MIN..MAX] of longint;

var
    v: tpVetor;
    n: longint;
    i: integer;

begin
    read(n);
    while n <> 0 do
    begin
        v[n mod 10] := v[n mod 10] + 1;
        n := n div 10;
    end;
    for i:= MIN to MAX do
        if v[i] <> 0 then
            writeln(v[i], ' digitos "', i, '"');
end.
        
