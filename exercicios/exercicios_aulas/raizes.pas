program raizes;
var
    n, count : integer;
    raiz : real;
begin
    readln(n);
    count := 1;
    while count <= n do
        begin
            raiz := sqrt(count);
            writeln(raiz:0:2);
            count := count + 1;
        end;
end.
