program p1ex1;
var
    km_l, tempo, velocidade, consumo: real;
begin
    read(km_l, tempo, velocidade);
    consumo := (tempo * velocidade) / km_l;
    writeln(consumo:0:2);
end.
