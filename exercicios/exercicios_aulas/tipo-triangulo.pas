program aula7ex1;
var
    l1, l2, l3 : integer;
begin
    readln(l1, l2, l3);
    if (l1 > l2 + l3) or (l2 > l1 + l3) or (l3 > l1 + l2) then
        writeln('valores invalidos')
    else if (l1 = l2) and (l2 = l3) then
        writeln('equilatero')
    else if (l1 = l2) or (l2 = l3) then
        writeln('isosceles')
    else
        writeln('escaleno');
end.
