program ex1;
var
    n: integer;
    par: boolean;
begin
    readln(n);
    while n <> 0 do
        begin
           par := (n mod 2) = 0;
           writeln('par: ', par);
           readln(n);
        end;
end.
