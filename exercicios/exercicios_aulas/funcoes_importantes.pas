procedure leVetor(var v: tpVetor; var tam: integer);
var i, n: integer;
begin
    read(n);
        i := 1;
        while n <> 0 do
        begin
            if n <> 0 then
            begin    
                v[i] := n;
                i := i + 1;
            end;
            read(n);
        end;
    tam := i;
end;    

procedure leVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i:= 1 to tam do
        read(v[i]);
end;
    
function buscaBinaria(n: integer; var v: tpVetor; tam: integer): integer;
var inicio, meio, fim: integer;
begin    
    inicio := 1;
    fim := tam;
    meio := (inicio + fim) div 2;
    while (v[meio] <> n) and (inicio <= fim) do
    begin
        if v[meio] > n then
            fim := meio - 1
        else
            inicio := meio + 1;
        meio := (inicio + fim) div 2;
    end;
    if inicio <= fim then
        buscaBinaria := meio
    else
        buscaBinaria := 0;
end;

function busca(n: integer; var v: tpVetor; tam: integer): integer;
var i: integer;
begin
    v[tam+1] := n;
    i := 1;
    while v[i] <> n do
        i := i + 1;
    if i <= tam then
        busca := i
    else
        busca := 0;
end;

procedure escreveVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
        writeln(v[i]);
end;

procedure ordenaVetor(var v: tpVetor; tam: integer);
var i, j, pos_menor, aux: integer;
begin
    for i:= 1 to tam-1 do
    begin
        pos_menor := i;
        for j := i + 1 to tam do
            if v[j] < v[pos_menor] then
                pos_menor := j;
        aux:= v[pos_menor];
        v[pos_menor] := v[i];
        v[i] := aux;
    end;
end;
