program palindromos;
var
    i, invertido, n: longint;
begin
    read(n);
    i := n;
    invertido := 0;
    while i > 0 do
    begin
        invertido := invertido * 10 + i mod 10;
        i := i div 10;
    end;
    writeln(invertido);
    if n = invertido then
        writeln('palindromo')
    else 
        writeln('nao');
end.
