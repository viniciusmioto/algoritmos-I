program fibonacci;
var
    i, penult, n, ult, fib: integer;
begin
    readln(n);
    i := 2;
    penult := 0;
    ult := 1;
    writeln('fib(0) = ', 0);
    writeln('fib(1) = ', 1);
    while i <= n do
    begin
        fib := ult + penult;
        writeln('fib(', i,') = ', fib);
        penult := ult;
        ult := fib;
        i := i + 1;
    end;
end.
