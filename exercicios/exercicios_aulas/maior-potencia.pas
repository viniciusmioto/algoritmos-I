program aula8ex3;
var
    n1, n2, maior_potencia: integer;
begin
    readln(n1, n2);
    if n2 mod n1 <> 0 then
        maior_potencia := 1
    else
    begin
        maior_potencia := n1;
        while n1 <= n2 do
        begin
            if n2 mod n1 = 0 then
            begin
                maior_potencia := n1;
                n1 := n1 * n1;
            end;
        end;
    end;
    writeln(maior_potencia);
end.
