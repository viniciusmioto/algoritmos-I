program aula23ex2;

const
    MAX = 100;

type
    tpMatriz = array[1..MAX, 1..MAX] of integer;
    tpVetor = array[1..MAX] of integer;
    
var
    matriz: tpMatriz;
    vetor: tpVetor;
    m, n: integer;
    
procedure leMatriz(var matriz: tpMatriz; m, n: integer);
var lin, col: integer;
begin    
    for lin := 1 to m do
        for col := 1 to n do
            read(matriz[lin][col]);
end;

procedure escreveMatriz(var matriz: tpMatriz; m, n: integer);
var lin, col: integer;
begin    
    for lin := 1 to m do
    begin
        for col := 1 to n do
            write(matriz[lin, col]:4);
        writeln();
    end;
end;

procedure preencheVetor(var vetor: tpVetor; matriz: tpMatriz; m, n: integer);
var lin, col, maior: integer;
begin
    for col := 1 to n do
    begin
        maior := matriz[1][col];
        for lin := 1 to m do
            if matriz[lin, col] > maior then
                maior := matriz[lin][col];
        vetor[col] := maior;
                
    end;
end;

procedure escreveVetor(var vetor: tpVetor; n: integer);
var i: integer;
begin
    for i:=1 to n do
        write(vetor[i], ' ');
    writeln();
end;
    
begin
    //m = linhas; n = colunas;
    read(m, n);
    leMatriz(matriz, m, n);
    writeln('MATRIZ:');
    escreveMatriz(matriz, m, n);
    preencheVetor(vetor, matriz, m, n);
    writeln('VETOR:');
    escreveVetor(vetor, n);
end.   
