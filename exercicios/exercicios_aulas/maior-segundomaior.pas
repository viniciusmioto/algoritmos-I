program aula8_ex1;
var
    n1, n2, maior, seg_maior: integer;
begin
    readln(n1);
    readln(n2);  
    if (n1 <> 0) or (n2 <> 0) then
    begin
        if n1 >= n2 then
        begin
            maior := n1;
            seg_maior := n2;
        end
        else
        begin
            maior := n2;
            seg_maior := n1;
        end; 
         
        while n1 <> 0 do
        begin
            if maior < n1 then
                maior := n1
            else if seg_maior <= n1 then
                seg_maior := n1;
            readln(n1);    
        end;
    end;
    writeln('maior: ', maior);
    writeln('segundo maior: ', seg_maior);
end.

