program aula7ex3;
var
    n: integer;
begin
    readln(n);
    while not((n mod 3 = 0) and (n mod 7 <> 0)) do
        readln(n);
    writeln(n);
end.
