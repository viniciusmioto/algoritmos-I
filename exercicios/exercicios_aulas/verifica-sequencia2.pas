program aula10ex2;
var
    k, n, numeros, i, j, verificador: integer;
    correta: boolean;
begin
    read(k, numeros);
    read(n);
    verificador := n mod 2;
    correta := true;
    i := 1;
    j := 1;
    while i <= numeros do
    begin
        while j <= k do
        begin
            if n mod 2 <> verificador then
                correta := false;
            j := j + 1;
            read(n);
        end;
        if verificador = 1 then
            verificador := 0
        else
            verificador := 1;
        i := i + 1;
    end;
    if correta then
        writeln('SIM')
    else
        writeln('NAO');
end.
