program somaDig;
var n: longint;

function soma_digitos(numero: longint):longint;
var soma: longint;
begin
    soma := 0;
    while numero <> 0 do
    begin
        soma := soma + numero mod 10;
        numero := numero div 10;
    end;
    soma_digitos := soma;
end;

begin
   read (n);
   while n <> 0 do
   begin   
      writeln ('soma dos digitos de ', n, ' = ', soma_digitos( n ));
      read( n );
   end;
end.
