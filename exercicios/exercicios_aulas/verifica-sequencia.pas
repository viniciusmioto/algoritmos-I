program aula9ex1;
var
    qtd, num, i, verificador: integer;
    valido: boolean;
begin
    readln(qtd);
    readln(num);
    valido := true;
    verificador := num mod 2;
    i := 1;
    while i < qtd do
    begin
        readln(num);
        if num mod 2 <> verificador then
            valido := false;
        i := i + 1;
    end;
    if valido then
        writeln(qtd)
    else
        writeln('sequencia invalida');
end.    

