program horario.pas;
var
    hora1, minuto1, hora2, minuto2,
    total_hora, total_minuto: integer;
begin
    readln(hora1, minuto1);
    readln(hora2, minuto2);
    total_minuto := minuto1 + minuto2;
    total_hora := hora1 + hora2 + total_minuto div 60;
    total_minuto := total_minuto mod 60;
    writeln(total_hora, ' ', total_minuto);
end.
