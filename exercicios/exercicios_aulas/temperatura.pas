program temperatura;
var
    celsius, fahrenheit : real;
begin
    readln(fahrenheit);
    celsius := 5/9 * (fahrenheit - 32);
    writeln(celsius:1:2);
end.
