program aula19ex3;
const
    MIN = 1;
    MAX = 10000;

type tpVetor = array [MIN..MAX] of longint;

var
    v, vConsecutivos: tpVetor;
    qtd: longint;
    i: integer;

procedure leSequencia(var vetor: tpVetor; tam: integer);
var i, n: integer;
begin
    j := 1;
    for i := 1 to tam do
    begin
        read(n);
        v[i] := v[i] + 1;
        if v[i-1] = v[i] then
            vConsecutivos[j] := v[i-1];
    end;        

end;

procedure contagem(var vetor: tpVetor);
begin
    for i:= MIN to MAX do
        if v[i] <> 0 then
            writeln(i, ' ocorre ', v[i], ' vezes');
end;

begin    
    read(qtd);
    for i:= MIN to MAX do
        v[i] := 0;
    leSequencia(v, qtd);
    contagem(v);
    
    for i:= 1 to qtd do
        write(v[i], ' ');
    for i := 1 to 4 do
        write(vConsecutivos[i], ' ');
end.
        

