program amigos_quadraticos;
var
    n, m: longint;
    
function soma_digitos(numero: longint):longint;
var soma: longint;
begin
    soma := 0;
    while numero <> 0 do
    begin
        soma := soma + numero mod 10;
        numero := numero div 10;
    end;
    soma_digitos := soma;
end;
    
begin
    read(n, m);
    writeln(soma_digitos(n*n), ' ', soma_digitos(m*m));
    while (n <> 0) or (m <> 0) do
    begin
        if (soma_digitos(n*n) = m) and (soma_digitos(m*m) = n) then
            writeln('amigos quadraticos')
        else
            writeln('nao sao amigos quadraticos');
       read(n, m);
    end;
end.
        
