program ex3;
var
    a, b, c : real;
begin
    readln(a, b, c);
    while (a <> 0) and (b <> 0) and (c <> 0) do
        begin
            writeln(c:0:2, ' ', b:0:2, ' ', a:0:2);
            readln(a, b, c);
        end;
end.
