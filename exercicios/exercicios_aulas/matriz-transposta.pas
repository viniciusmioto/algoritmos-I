program aula23ex2;

const
    MAX = 100;

type
    tpMatriz = array[1..MAX, 1..MAX] of integer;
    
var
    matriz: tpMatriz;
    m, n: integer;
    
procedure leMatriz(var matriz: tpMatriz; m, n: integer);
var lin, col: integer;
begin    
    for lin := 1 to m do
        for col := 1 to n do
            read(matriz[lin][col]);
end;

procedure escreveTransposta(var matriz: tpMatriz; m, n: integer);
var lin, col: integer;
begin    
    for col := 1 to n do
    begin
        for lin := 1 to m do
            write(matriz[lin, col]:4);
        writeln();
    end;
end;

procedure escreveMatriz(var matriz: tpMatriz; m, n: integer);
var lin, col: integer;
begin    
    for lin := 1 to m do
    begin
        for col := 1 to n do
            write(matriz[lin, col]:4);
        writeln();
    end;
end;

begin
    //m = linhas; n = colunas;
    read(m, n);
    leMatriz(matriz, m, n);
    writeln('MATRIZ:');
    escreveMatriz(matriz, m, n);
    writeln('TRANSPOSTA:');
    escreveTransposta(matriz, m, n);
end.
