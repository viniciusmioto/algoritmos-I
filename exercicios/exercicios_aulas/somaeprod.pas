program soma_e_produto;
var
    b, c : integer;
    n1, n2, delta: real;
begin
    readln(b, c);
    delta := sqrt(b * b - 4 * c);
    n1 := (b - delta)/2;
    n2 := (b + delta)/2;
    writeln(n1:0:2, ' ', n2:0:2);
end.
