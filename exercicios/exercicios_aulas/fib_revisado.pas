program fib_revisado;
//imprime o primeiro fibonacci maior que n
var
    n, ultimo, penultimo, fib: integer;
begin
    read(n);
    ultimo := 1;
    penultimo := 0;
    fib := 1;
    while fib <= n do
    begin
        fib := ultimo + penultimo;
        penultimo := ultimo;
        ultimo := fib;
    end;
    writeln(fib);
end.
