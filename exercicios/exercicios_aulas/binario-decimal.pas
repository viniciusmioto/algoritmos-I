program aula10ex1;
var
    binario, decimal: longint;
    i: integer;
begin
    read(binario);
    decimal := 0;
    i := 1;
    while binario > 0 do
    begin
        decimal := decimal + binario mod 10 * i;
        binario := binario div 10;
        i := i * 2;
    end;
    writeln(decimal);
end.
