program aula19ex4;
const
    MIN = 1;
    MAX = 100;
    
type
    tpVetor = array[MIN..MAX] of longint;

var
    v: tpVetor;
    qtd, i, m, tamConsec: integer;
    
procedure leVetor(var v: tpVetor; tam: integer; var tamConsec, i, m: integer);
var 
    n, j, c: integer;
    vConsecutivos: tpVetor;
begin
    j := 0;
    tamConsec := 1;
    i := 0;
    for c := 1 to tam do
    begin
        read(n);
        v[c] := n;
        if (v[c-1] = v[c]+1) xor (v[c-1] = v[c]-1) then
        begin
            if i = 0 then
                i := c-1;
            j := j + 1;
            tamConsec := tamConsec + 1;
            vConsecutivos[j] := v[c-1];
            vConsecutivos[j+1] := v[c]	;
        end;
    end; 
    m := tamConsec div 2;
end;

begin
    read(qtd);
    leVetor(v, qtd, tamConsec, i, m);  
    writeln(i, ' ', m);
end.
