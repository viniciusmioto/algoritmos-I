program aula18ex3;
const
    MIN = 2;
    MAX = 1000;
type
    tpVetorBool = array[MIN..MAX] of boolean;
var
    primos: tpVetorBool;
    i: integer;
    
procedure inicializaVetor(var v: tpVetorBool);
var i: integer;
begin
    for i:= MIN to MAX do
        v[i] := true;
end;

procedure marcaMultiplos(var primos: tpVetorBool; p: integer);
var i: integer;
begin
    i := p*2;
    while i <= MAX do
    begin
        primos[i] := false;
        i:= i + p;
    end;
end;
    
begin
    inicializaVetor(primos);
    for i := MIN to MAX do
        if primos[i] then
        begin
            marcaMultiplos(primos, i);
            writeln(i);
        end;
end.
