program aula7ex4;
var
    n1, n2, cont: integer;
begin
    readln(n1, n2);
    cont := 0;
    while n1 >= n2 do
    begin
        n1 := n1 - n2;
        cont := cont + 1;
    end;
    writeln('mod = ', n1);
    writeln('div = ', cont);
end.
