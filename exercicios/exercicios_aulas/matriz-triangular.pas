program aula23ex1;

type
    tpMatriz = array[1..40, 1..40] of integer;
    
var
    triangulo: tpMatriz;
    n: integer;

procedure inicializaTriangulo(var triangulo: tpMatriz; n: integer);
var linha, coluna: integer;
begin
    for linha:= 1 to n do
        for coluna:= 1 to n do
        begin
            if (coluna = 1) or (linha = coluna) then
                triangulo[linha][coluna] := 1
            else
                 triangulo[linha][coluna] := 0;
        end;     
    
    for linha := 1 to n do
    begin
        coluna := 1;
        while coluna <= linha do
        begin
            if triangulo[linha][coluna] = 0 then
                triangulo[linha][coluna] := triangulo[linha-1][coluna] + triangulo[linha-1][coluna-1];
            coluna := coluna + 1;
        end;        
    end;
end;

procedure escreveTriangulo(var triangulo: tpMatriz; n: integer);
var linha, coluna: integer;
begin
    for linha:= 1 to n do
    begin
        coluna := 1;
        while coluna <= linha do
        begin
            write(triangulo[linha, coluna]:4);
            coluna := coluna + 1;
        end;
         writeln();     
    end;
end;
    
begin
    read(n);
    if n <= 40 then
    begin
        inicializaTriangulo(triangulo, n);
        escreveTriangulo(triangulo, n);
    end;
end.
