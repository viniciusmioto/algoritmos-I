program ex18;
var
    A, B, C, X1, X2 : real;
begin
    writeln('EQUACAO DO 2o GRAU');
    while true do
        begin 
            write('Informe A, B, C: ');
            readln(A, B, C);
            if (A = 0) and (B = 0) and (C = 0) then
                break;
            X1 := ((-B + sqrt(B*B - 4*A*C))/2*A);
            X2 := ((-B - sqrt(B*B - 4*A*C))/2*A);
            writeln(X1:2:2, ' ',X2:2:2);
    end;
end.
