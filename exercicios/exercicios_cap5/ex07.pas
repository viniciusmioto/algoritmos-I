program exerc7;
var
    a, b: integer;
begin
    readln(a,b);
    a := b + a;
    b := a - b;
    a := a - b;
    writeln(a, ' ', b);
end.
