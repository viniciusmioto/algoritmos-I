program exerc8.pas;
var 
    a, d, p, q, r: integer;
    b, z: real;
    sim : boolean;

begin
    sim := true;
    a := 6;
    b := 9.5;
    d := 14;
    p := 4;
    q := 5;
    r := 10;
    z := 6.0;
    
    write('Item A: ');
    writeln(sim AND (q >= p));
    // TRUE and (5 >= 4)
    // TRUE and TRUE  = TRUE    

    write('Item B: ');
    writeln((0 <= b) AND (z > a) OR (a = b));
    // (0 <= 9.5) and (6.0 > 6) or (6 = 9.5)
    // TRUE and FALSE or FALSE = FALSE

    write('Item C: ');
    writeln((0 <= b) AND ((z > a) OR (a = b)));
    // (0 <= 9.5) and ((6.0 > 6) or (6 = 9.5))
    // TRUE and (FALSE or FALSE)
    // TRUE and FALSE = FALSE

    write('Item D: ');
    writeln((0 <= b) OR ((z > a) AND (a = b)));
    // (0 <= 9.5) or ((6.0 > 6) and (6 = 9.5))
    // TRUE or (FALSE and FALSE)
    // TRUE or FALSE = TRUE    


 //Obs.: no livro aparecem os códigos:
 //"\le" que significa menor ou igual a (<=)
 //"\ge" que significa maior ou igual a (>=)    

end.

