program exerc9;
var
    a, b, d, p, q, r, x, y, z: integer;
    sim : boolean;
    result : real;

begin
    sim := true;
    a := 5;
    b := 3;
    d := 7;
    p := 4;
    q := 5;
    r := 2;
    x := 8;
    y := 4;
    z := 6;

    write('Item A: ');
    writeln((z div a + b * a) - d div 2);
    // (6 div 5 + 3 * 5) - 7 div 2
    // (1 + 15) - 3 
    // 16 - 3 = 13


    write('Item B: ');
    writeln((2 mod q - q / 2):2:2);
    // p / r mod q - q / 2
    // 4 / 2 mod 5 - 5 / 2
    // 2 mod 5 - 5 / 2
    // 2 - 2.5 = - 0.5

    write('Item C: ');
    writeln((z div y + 1 = x) and sim or (y >= x));
    // (z div y + 1 = x) and sim or (y >= x)
    // (6 div 4 + 1  = 8) and TRUE or (4 >= 8)
    // (1 + 1 = 8) and TRUE or FALSE
    // FALSE and TRUE = FALSE    

// Obs.: As variáveis desse exercícios foram
// declaradas com os valores do exercício 10.

end.

