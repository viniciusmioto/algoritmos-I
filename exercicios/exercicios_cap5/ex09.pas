program exerc9;
var
    a, b, d, p, q, r, x, y, z: integer;
    sim : boolean;

begin
    sim := true;
    a := 5;
    b := 3;
    d := 7;
    p := 4;
    q := 5;
    r := 2;
    x := 8;
    y := 4;
    z := 6;

    write('Item A: ');
    writeln(NOT sim AND (z DIV b + 1 = r));
    // not TRUE and (6 div 3 + 1 = 2)
    // FALSE and (2 + 1 = 2)
    // FALSE and FALSE = FALSE

    write('Item B: ');
    writeln((x + y > z) AND sim OR (d >= b));
    // (8 + 4 > 6) and TRUE or (7 >= 3)
    // (12 > 6) and TRUE or TRUE
    // TRUE and TRUE or TRUE = TRUE

    write('Item C: ');
    writeln((x + y <> z) AND (sim OR (d >= b)));
    // (8 + 4 <> 6) and (TRUE or (7 >= 3))
    // (12 <> 6) and (TRUE or TRUE)
    // TRUE and TRUE = TRUE


// Obs.: As variáveis desse exercícios foram
// declaradas com os valores do exercício 10.

end.

