program exerc3b;
var
    p1, p2, r, y, z, w, a, b: integer;
    x: real;
begin
    writeln('Informe P1, P2, R, Y, Z , W, A, B:');
    readln(p1, p2, r, y, z, w, a, b);
    x := (((p1 + p2)/(y - z)) * r) / ((w / (a * b)) + r);
    writeln(x:4:2);
end.

