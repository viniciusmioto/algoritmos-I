program exerc11;
var
    m, x, y: integer;
begin
    read(x);
    y := 0;
    m := 1;
    while x > 0 do
        begin
            y:= y + (x mod 2) * m;
            x:= x div 2;
            m:= m * 10;
            writeln('x = ', x);
            writeln('y = ', y);
            writeln('m = ', m);
            writeln();
        end;
    writeln(y)
end.
// para x = 0, o programa escreverá 0, pois,
// no primeiro teste lógico (WHILE 0 > 0) tem-se
// o resultado FALSO, logo, não executará o laço de repetição.
// sendo assim, o valor de y permanece ZERO.

// para x = 10, o programa escreverá 1010, pois
// WHILE (10 > 0) retorna VERDADEIRO, e nessa execução tem-se:
// y = 0; x = 5; m = 10;
// WHILE (5 > 0) retorna VERDADEIRO, e nessa execução tem-se:
// y = 10; x = 2; m = 100;
// WHILE (2 > 0) retorna VERDADEIRO, e nessa execução tem-se:
// y = 10; x = 1; m = 1000;
// WHILE (1 > 0) retorna VERDADEIRO, e nessa execução tem-se:
// y = 1010; x = 0; m 10000;
// WHILE(0 > 0) retorna FALSE, e o laço se encerra com os valores
// da ultima execução, assim y = 1010.

// para x = -1, acontece como no primeiro caso
// WHILE(-1 > 0) retorna FALSO, logo não executará o laço de repetição
// sendo assim, o valo de y permanece ZERO
