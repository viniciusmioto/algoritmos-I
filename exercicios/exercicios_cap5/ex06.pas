program exerc6;
var
    a, b, aux : integer;
begin
    readln(a, b);
    aux := a;
    a := b;
    b := aux;
    writeln(a, ' ', b);
end.

