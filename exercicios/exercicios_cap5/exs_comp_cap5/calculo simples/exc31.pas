program exc31;
var
    sal_minimo, quilowatts, val_kw, conta : real;
begin
    write('Informe o Salario Minimo: ');
    readln(sal_minimo);
    write('Informe a quntidade de quilowatts: ');
    readln(quilowatts);
    val_kw := (sal_minimo/7)/100;
    conta := quilowatts * val_kw;
    writeln('Valor do KW: ', val_kw:2:2);
    writeln('Conta = R$', conta:2:2);
end.
