program exc29;
var
    total, salario : real;
    sapatos : integer;
begin
    write('Informe o valor total em vendas: ');
    readln(total);
    write('Informe a quantidade de sapatos vendidos: ');
    readln(sapatos);
    salario := total * 0.20 + sapatos * 5;
    writeln('Salario = ', salario:2:2);
end.

