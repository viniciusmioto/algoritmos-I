program exc28;
var
    tempo, velocidade, distancia, litros : real;
begin
    write('Informe o tempo gasto: ');
    readln(tempo);
    write('Informe a velocidade media: ');
    readln(velocidade);
    distancia := velocidade * tempo;
    litros := distancia / 12;
    writeln('Distancia percorrida: ', distancia:2:2, ' km');
    writeln('Litros consumidos: ', litros:2:2, ' L');
end.

