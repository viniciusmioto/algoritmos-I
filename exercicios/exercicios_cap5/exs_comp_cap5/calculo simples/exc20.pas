program exc20;
var
    sn, a1, q, n : real;
begin
    write('Informe A1, Q, N: ');
    readln(a1, q, n);
    sn := (a1 * (exp(n * ln(q)) - 1)) / (q - 1);
    writeln(sn:4:2);
end.

