program exc03;
var
    valor_original, desconto: real;
begin
    write('Valor orginal: ');
    readln(valor_original);
    write('Qual o desconto? :');
    readln(desconto);
    writeln('Valor na promocao: ', (valor_original - desconto):2:2);
end.
