program ex6;
var
    n, a1, an: integer;
    sn: real;
begin
    write('N: ');
    readln(n);
    write('A1: ');
    readln(a1);
    write('An: ');
    readln(an);
    sn := ((a1+ an) * n)/2;
    writeln(sn:8:2);
end.
