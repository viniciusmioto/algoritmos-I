program exc21;
var
    v, r, h : real;
begin
    write('Informe o raio e a altura: ');
    readln(r, h);
    v := 3.14159 * r * r * h;
    writeln(v:2:2);
end.
