program exc23;
var
    valor_prod, novo_valor, desconto: real;
begin
    write('Valor do produto: ');
    readln(valor_prod);
    novo_valor := valor_prod * 0.91;
    desconto := valor_prod - novo_valor;
    writeln('Novo valor  = ', novo_valor:2:2);
    writeln('Desconto = ', desconto:2:2);
end.

