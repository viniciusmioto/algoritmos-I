program exc22;
var
    valor_hora, num_aulas, perc_inss, salario_bruto: real;
begin
    write('Informe o valor hora aula: ');
    readln(valor_hora);
    write('Informe o numero de aulas: ');
    readln(num_aulas);
    write('Informe o percentual do INSS: ');
    readln(perc_inss);
    salario_bruto := valor_hora * num_aulas * (100 - perc_inss)/100;
    writeln('Salario Bruto = ', salario_bruto:4:2);
end.

