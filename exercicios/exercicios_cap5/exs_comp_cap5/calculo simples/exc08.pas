program exc08;
var
    a1, q, n: integer;
begin
    write('A1: ');
    readln(a1);
    write('Q: ');
    readln(q);
    write('N: ');
    readln(n);
    // an = a1 * q ^ (n-1)
    writeln((a1 * exp((n-1) * ln(q))):8:2);
end.
