program exc09;
var
    k, ak, q, n: integer;
begin
    write('K: ');
    readln(k);
    write('Ak: ');
    readln(ak);
    write('Q: ');
    readln(q);
    write('N: ');
    readln(n);
    // an = a1 * q ^ (n-1)
    writeln((ak * exp((n-k) * ln(q))):8:2);
end.
