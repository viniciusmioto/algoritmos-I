program exc06;
var
    an, a1, n, r: integer;
begin
    write('N: ');
    readln(n);
    write('R: ');
    readln(r);
    write('A1: ');
    readln(a1);
    an := a1 + (n - 1) * r;
    writeln('An = ', an);
end.
