program exc26;
var
    deposito, taxa, rendimento, total : real;
begin
    write('Informe o Deposito: ');
    readln(deposito);
    write('Informe a Taxa Taxa %: ');
    readln(taxa);
    rendimento := deposito * taxa/100;
    total := deposito + rendimento;
    writeln('------------------');
    writeln('RENDIMENTO | TOTAL');
    writeln(deposito:10:2, ' | ', total:6:2);
end.
