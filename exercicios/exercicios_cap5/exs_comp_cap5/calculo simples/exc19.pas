program exc19;
var
    p, a1, q, n: real;
begin
    write('Informe A1, Q, N: ');
    readln(a1, q, n);
    // p = (a1^n) * q^((n * (n-1))/2)
    p := exp(n * ln(a1)) * exp(((n*(n-1)) * ln(q))/2);
    writeln(p:4:2);
end.

