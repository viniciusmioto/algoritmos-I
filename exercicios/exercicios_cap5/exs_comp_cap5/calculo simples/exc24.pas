program exc24;
var
    conta, gorjeta, total : real;
begin
    write('Conta: ');
    readln(conta);
    gorjeta := conta * 0.1;
    total := conta + gorjeta;
    writeln('Gorjeta = ', gorjeta:2:2);
    writeln('Conta = ', total:2:2);
end.
