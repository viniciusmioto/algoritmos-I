program exc10.pas;
var
    n: integer;
begin
    readln(n);
    if n >= 0 then
        writeln(sqrt(n):0:2)
    else
        writeln(n * n);
end.

