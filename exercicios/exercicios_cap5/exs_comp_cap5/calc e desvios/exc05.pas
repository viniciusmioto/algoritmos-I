program exc05;
var
    n1, n2, resultado : real;
begin
    readln(n1, n2);
    resultado := n1 + n2;
    if resultado > 20 then
        writeln(resultado + 8:1:2)
     else
        writeln(resultado - 5:1:2);
end.

