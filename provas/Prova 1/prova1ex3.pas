program prova1ex3;
var
    n, copia, i, d, divisor, divisor_primo, potencia, resultado, cont, maior_divisor: longint;
    primo: boolean;
begin
    read(n);
    d := 2;
    potencia := 1;
    resultado := 1;
    maior_divisor := 1;
    primo := true; 
    if n > 1 then
    begin
    
        while d <= n do 
        begin
            //encotrar os divisores de n
            if n mod d = 0 then
            begin
                divisor := d;
                
                //testar se esse divisor é primo
                i := 2;
                          
                while i <= sqrt(divisor) do
                begin
                    if divisor mod i = 0 then
                        primo := false;
                    i := i + 1;
                end;
                if primo then 
                        divisor_primo := divisor;        
                primo := true;
                
                //testar o maior divisor
                potencia := 1;
                copia := n;
                while copia mod divisor_primo = 0 do
                begin
                    copia := copia div divisor_primo;
                    if copia mod divisor_primo = 0 then   
                        potencia := potencia + 1;
                end;
                cont := 1;
                resultado := 1;
                while cont <= potencia do
                begin
                    resultado := resultado * divisor_primo;
                    cont := cont + 1;
                end;
                
                if resultado > maior_divisor then
                    maior_divisor := resultado;
            end;              
            d := d + 1;
        end;
    end;
    writeln(maior_divisor);
end.
