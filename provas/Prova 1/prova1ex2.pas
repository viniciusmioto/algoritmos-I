program prova1ex2;
var
    n, mais_divs, divisores, qtd_anterior, i: integer;
begin
    read(n);
    mais_divs := 0;
    qtd_anterior := 0;
    divisores := 0;
    while n <> 0 do
    begin
        i := 1;
        while i <= n do
        begin
            if (n mod i) = 0 then
                divisores := divisores + 1;
            i := i + 1;
        end;
        
        if divisores > qtd_anterior then
        begin
            mais_divs := n;
            qtd_anterior := divisores;
        end;
        
        divisores := 0;
        read(n);
        
    end;
    writeln(mais_divs);
end.
