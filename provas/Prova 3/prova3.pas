program matrizes;

type
    matriz = array[1..100, 1..100] of integer;
    
var
    n, tam, l_ini, c_ini, l_final, c_final: integer;   
    encontrou: boolean; 
    m: matriz;
    
procedure le_matriz(var m: matriz; n: integer);
var l, c: integer;
begin
    for l := 1 to n do
        for c := 1 to n do
            read(m[l, c]);
end;    

procedure imprime_matriz(var m: matriz; n: integer);
var l, c: integer;
begin
    for l := 1 to n do
    begin
        for c := 1 to n do
            write(m[l, c], ' ');
        writeln();
    end;
end;    
    
procedure preenche_matriz(var m: matriz; n: integer);
var l, c: integer;
begin
    for l := 1 to n do
        for c := 1 to n do
            m[l, c] := l * 10 + c;
end;    
    
procedure calcula_final(tam, n: integer; var l_final, c_final: integer);
begin    
    l_final := n - tam + 1;
    c_final := n - tam + 1;
end;
   
function verifica_1(var m: matriz; tam, l_ini, c_ini: integer): boolean;
var 
    l, c: integer;
    tem_zero: boolean;
begin
    l := l_ini;
    tem_zero := false;
    while (l <= l_ini + tam - 1) and not (tem_zero) do
    begin
        c:= c_ini;
        while (c <= c_ini + tam - 1) and not (tem_zero) do
        begin
            if m[l, c] <> 1 then
                tem_zero := true;
            c := c + 1;
        end;
        l := l + 1;
    end;
    verifica_1 := not(tem_zero);
end;
    
procedure percorre_quadrada(var m: matriz; tam: integer; var encontrou: boolean; var l_ini, c_ini, l_final, c_final: integer);
var l, c: integer;
begin
    encontrou := false; 
    calcula_final(tam, n, l_final, c_final);
    while (l_ini <= l_final) and not(encontrou) do
    begin
        c_ini := 1;
        while (c_ini <= c_final) and not(encontrou) do
        begin
            l:= l_ini;
            c:= c_ini;
            if verifica_1(m, tam, l_ini, c_ini) then
                encontrou := true;
            c_ini := c_ini + 1;
        end;
        l_ini := l_ini + 1;
    end;
    l_ini := l;
    c_ini := c;
end;  
    
begin
    read(n);
    le_matriz(m, n);
    tam := n;
    encontrou := false;
    while (tam >= 1) and not(encontrou) do
    begin
        l_ini := 1;
        c_ini := 1;
        l_final := 0;
        c_final := 0; 
        percorre_quadrada(m, tam, encontrou, l_ini, c_ini, l_final, c_final);
        tam := tam - 1;
    end;
    writeln('(', l_ini, ', ',  c_ini, ') e (', l_final, ', ', c_final, ') ');
    
end.



