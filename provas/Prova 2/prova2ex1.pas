program exercicio1;
//versao 2

type
    tpVetor = array[1..200] of integer;
    
var
    n, m: integer;
    vetor1, vetor2: tpVetor;
    
function busca(n: integer; var v: tpVetor; tam: integer): integer;
var i: integer;
begin
    v[tam+1] := n;
    i := 1;
    while v[i] <> n do
        i := i + 1;
    if i <= tam then
        busca := i
    else
        busca := 0;
end;    
    
procedure leVetor(var v: tpVetor; tam: integer);
// nessa versao o procedimento "leVetor" somente irá inserir caso o valor N não esteja no vetor
var i: integer;
begin
    i := 0;
    while i < tam do
    begin
        read(n);
        if busca(n, v, tam) = 0 then
        begin
            i := i + 1;
            v[i] := n;
        end;
    end;
end;    
    
function encontrouInterseccao(var vetor1, vetor2: tpVetor; tam1, tam2: integer): boolean;
var 
    i: integer;
    encontrou: boolean;
begin
    encontrou := false;
    for i := 1 to tam1 do
        if busca(vetor1[i], vetor2, tam2) <> 0 then
            encontrou := true;
    encontrouInterseccao := encontrou;
end;
    
begin
    read(n, m);
    //ler dois vetores
    leVetor(vetor1, n);
    leVetor(vetor2, m);
    
    //verificar se existe interseccao 
    writeln(encontrouInterseccao(vetor1, vetor2, n, m));
end.
