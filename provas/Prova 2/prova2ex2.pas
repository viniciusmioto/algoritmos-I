program exercicio2;

type
    tpVetor = array[1..200] of integer;

var
    v, w: tpVetor;
    tam, tam_w, i: integer;
    limite_soma: longint;

procedure leVetor(var v: tpVetor; var tam: integer);
var i, n: integer;
begin
    read(n);
        i := 0;
        while n <> 0 do
        begin
            if n > 0 then
            begin    
                i := i + 1;
                v[i] := n;
            end;
            read(n);
        end;
    tam := i;
end; 

procedure preenche_w(var v, w: tpVetor; tam: integer; var inicio: integer; limite_soma: longint; var tam_w: integer);
var i, j, soma: integer;
begin
    soma := 0;
    j := tam_w;
    i := inicio;
    while (i <= tam) and (soma < limite_soma) do
    begin
        soma := soma + v[i];
        if soma > limite_soma then
        begin   
            j := j + 1;
            w[j] := v[i];
        end;
        i := i + 1;
        inicio := i;
    end;
    tam_w := j;
end;

procedure escreveVetor(var v: tpVetor; tam: integer);
var i: integer;
begin
    for i := 1 to tam do
        write(v[i], ' ');
    writeln();
end;   

begin
    tam := 0;
    tam_w := 0;
    leVetor(v, tam);
    read(limite_soma);
    i := 1;
    while i <= tam do
        preenche_w(v, w, tam, i, limite_soma, tam_w);
    escreveVetor(w, tam_w);
end.
