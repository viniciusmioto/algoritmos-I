# Algoritmos e Estrutura de Dados I
---
Este repositório foi criado com o objetivo de organizar os exercícios resolvidos dessa disciplina.<br />
Todas as atividades e programas foram realizados na linguagem _**Pascal**_, seguindo as aulas da UFPR.<br />

Neste repositório você encontrará:
- **Livro de Algoritimo e Estrutura de Dados I**;  <br />
    Escrito por:
    - Marcos Castilho, 
    - Fabiano Silva,
    - Daniel Weingaertner.  <br />
    [Livro Alg I](http://www.inf.ufpr.br/cursos/ci055/livro_alg1.pdf) <br />
    
- **Guia da Rápido de Referência da Linguagem Pascal**;  <br />
    Escrito por: 
   - Marcos Castilho, 
   - Everaldo Gomes, 
   - Loirto Alves dos Santos, 
   - Eleandro Maschio Krynski ,
   - José Ivan Gonçalves Júnior, 
   - Rene Kultz.  <br />
   [Guia de Pascal](http://www.inf.ufpr.br/cursos/ci055/pascal.pdf) <br />
 - **Exercícios em Linguagem Pascal**  <br />
 [Guia de Linux](http://www.inf.ufpr.br/cursos/ci055/linux.pdf) <br />
  
   Acesse a [Página Oficial](http://www.inf.ufpr.br/cursos/ci055/) da disciplina.
---   
Aluno: **Vinícius Mioto** <br />
Curso: **Ciência da Computação** <br />
**Universidade Federal do Paraná** <br />
